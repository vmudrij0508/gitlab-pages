import gulp from "gulp";
import uglify from "gulp-uglify";
import concat from "gulp-concat";
import imagemin from "gulp-imagemin";

import dartSass from "sass";
import gulpSass from "gulp-sass";

const sass = gulpSass(dartSass);

function html(done) {
  gulp.src("src/*.html").pipe(gulp.dest("dist")).on("end", done);
}

function images(done) {
  gulp
    .src("./src/images/*")
    .pipe(imagemin())
    .pipe(gulp.dest("./dist/images"))
    .on("end", done);
}

function js(done) {
  gulp
    .src("src/js/**/*.js")
    .pipe(uglify())
    .pipe(concat("main.js"))
    .pipe(gulp.dest("dist/js"))
    .on("end", done);
}

function scss(done) {
  gulp
    .src("src/sass/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("dist/css"))
    .on("end", done);
}

const build = gulp.series(html, gulp.parallel(images, js, scss));

export { html, images, js, scss, build };

export default build;
