function tax(price) {
  return price * 0.07;
}

function shipping(price) {
  return price * 0.15;
}

function total(price, tax, shipping) {
  return price + tax + shipping;
}

const checkout = {
  price,
  tax,
  shipping,
  total,
};
