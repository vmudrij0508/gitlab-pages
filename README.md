# GitLab Pages

This documentation will guide you through the process of setting up GitLab CI/CD for Pages. By following the steps below, you will be able to add a .gitlab-ci.yml file to your project root, commit and push the changes to GitLab, and finally, view the deployed site.

## Step 1: Add .gitlab-ci.yml to Project Root

Create a new file named `.gitlab-ci.yml` in the root directory of your project. This file will contain the configuration for your GitLab CI/CD pipeline.

## Step 2: Commit the Changes

After creating the `.gitlab-ci.yml` file, commit it to your project:

```bash
git add .gitlab-ci.yml
git commit -m "Add .gitlab-ci.yml for GitLab Pages"
```

## Step 3: Push the Commit to GitLab

Push the commit containing the `.gitlab-ci.yml` file to your GitLab repository:

```bash
git push origin <your-branch>
```

Replace `<your-branch>` with the name of the branch you are working on.

## Step 4: View the CI/CD Job

Navigate to your repository on GitLab, then go to the "CI/CD" section and click on "Jobs." You should see a job listed. Monitor the job's progress and wait for the status to change to "passed."

## Step 5: Access the Deployed Site

Once the job has passed, go to the "Deployment" section in your GitLab repository and click on "Pages." You will find the URL of your deployed site here. Open the link in your web browser to view your deployed site.

By following these steps, you have successfully set up GitLab CI/CD for Pages and deployed your site using GitLab Pages.
